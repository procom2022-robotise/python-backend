from bibliotheques.sql.sql import *

input_maj_machine = "0,None,None/1,6,0/2,2,0/3,None,None/4,None,None/5,None,None" #id_bec,id_ingredient,quantite
input_command_machine = "0,0,3/0,3,6/0,4,10"

print(mysql_get_cocktail_list())
print(mysql_get_ingredient_list())
print(mysql_get_quantite_machine())
print(mysql_get_all_cocktail_composition_without_bec())
print(mysql_get_cocktail_composition_without_bec(1))
print(mysql_get_cocktail_composition_with_bec(1))
print(mysql_get_bec_from_ingredient(1))
mysql_add_ingredient("test")
mysql_maj_machine(unformalize_data_list(input_maj_machine))
mysql_get_composition_for_machine(unformalize_data_list(input_command_machine))