import mysql.connector

def connect_db():
    db = mysql.connector.connect(
        user='robotise',
        password='a693bc7501ca6755ce67329132f581ca',
        host='localhost',
        database='robotise')
    return db

def mysql_get_cocktail_list():
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM cocktail")
    data = cursor.fetchall()
    cursor.close()
    connection.close()
    return data

def mysql_get_ingredient_list():
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM ingredient")
    data = cursor.fetchall()
    cursor.close()
    connection.close()
    return data

def mysql_get_quantite_machine():
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM machine")
    data = cursor.fetchall()
    cursor.close()
    connection.close()
    return data

def mysql_get_all_cocktail_composition_without_bec():
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute("SELECT cocktail.id_cocktail, nom_cocktail, ingredient.id_ingredient, nom_ingredient, quantite FROM composition INNER JOIN cocktail ON composition.id_cocktail = cocktail.id_cocktail INNER JOIN ingredient ON composition.id_ingredient = ingredient.id_ingredient",)
    data = cursor.fetchall()
    cursor.close()
    connection.close()
    return data

def mysql_get_cocktail_composition_without_bec(id_cocktail: int):
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute("SELECT cocktail.id_cocktail, nom_cocktail, nom_ingredient, quantite FROM composition INNER JOIN cocktail ON composition.id_cocktail = cocktail.id_cocktail INNER JOIN ingredient ON composition.id_ingredient = ingredient.id_ingredient WHERE cocktail.id_cocktail = %s", (id_cocktail,))
    data = cursor.fetchall()
    cursor.close()
    connection.close()
    return data

def mysql_get_cocktail_composition_with_bec(id_cocktail: int):
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute("SELECT cocktail.id_cocktail, nom_cocktail, nom_ingredient, quantite, id_bec FROM composition INNER JOIN cocktail ON composition.id_cocktail = cocktail.id_cocktail INNER JOIN ingredient ON composition.id_ingredient = ingredient.id_ingredient WHERE cocktail.id_cocktail = %s", (id_cocktail,))
    data = cursor.fetchall()
    cursor.close()
    connection.close()
    return data

def mysql_get_bec_from_ingredient(id_ingredient: int):
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute("SELECT id_bec FROM ingredient WHERE id_ingredient = %s", (id_ingredient,))
    data = cursor.fetchall()
    cursor.close()
    connection.close()
    return data

def mysql_add_ingredient(nom_ingredient: str):
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute("INSERT INTO ingredient (id_bec, nom_ingredient) VALUES (%s, %s)", (None, nom_ingredient,))
    connection.commit()
    cursor.close()
    connection.close()

def mysql_maj_machine(maj: tuple):
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute("UPDATE ingredient SET id_bec = NULL")
    for k in range(6):
        cursor.execute("UPDATE machine SET quantite = %s WHERE id_bec = %s", (maj[k][2], maj[k][0],))
        cursor.execute("UPDATE ingredient SET id_bec = %s WHERE id_ingredient = %s", (maj[k][0], maj[k][1],))
    connection.commit()
    cursor.close()
    connection.close()

def mysql_get_composition_for_machine(commande: tuple):
    data = []
    connection = connect_db()
    cursor = connection.cursor()
    for k in commande:
        cursor.execute("SELECT machine.id_bec FROM ingredient INNER JOIN machine ON ingredient.id_bec = machine.id_bec WHERE ingredient.id_ingredient = %s", (k[1],))
        data_requete = cursor.fetchall()
        if len(data_requete)==0:
            cursor.close()
            connection.close()
            data = None
            break
        else:
            bec=data_requete[0][0]
            data.append((bec, int(k[2])))
    cursor.close()
    connection.close()
    return data

def formalize_data_list(function):
    initial_data = function()
    new_data = "/".join([",".join([str(k) for k in x]) for x in initial_data])
    return str(new_data)

def unformalize_data_list(initial_data: str):
    sublists = initial_data.split("/")
    sublist_data = [x.split(",") for x in sublists]
    new_data = [[None if 'None' in k else k for k in x] for x in sublist_data]
    return tuple(new_data)
