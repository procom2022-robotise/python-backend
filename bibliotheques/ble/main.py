import sys
import dbus
from .advertisement import Advertisement, register_ad_cb, register_ad_error_cb
from .gatt_server import Service, Descriptor, Characteristic, GATT_DESC_IFACE, register_app_cb, register_app_error_cb
from gi.repository import GLib, GObject
import time

from ..machine.script_flowmeter import *

from ..sql.sql import *

BLUEZ_SERVICE_NAME =           'org.bluez'
DBUS_OM_IFACE =                'org.freedesktop.DBus.ObjectManager'
LE_ADVERTISING_MANAGER_IFACE = 'org.bluez.LEAdvertisingManager1'
GATT_MANAGER_IFACE =           'org.bluez.GattManager1'
GATT_CHRC_IFACE =              'org.bluez.GattCharacteristic1'
UART_SERVICE_UUID =            '6e400001-b5a3-f393-e0a9-e50e24dcca9e'
UART_RX_CHARACTERISTIC_UUID =  '6e400002-b5a3-f393-e0a9-e50e24dcca9e'
UART_TX_CHARACTERISTIC_UUID =  '6e400003-b5a3-f393-e0a9-e50e24dcca9e'
LOCAL_NAME =                   'robotise'
CLIENT_UUID =                  '00002902-0000-1000-8000-00805f9b34fb'

SQL_GET_COCKTAIL_LIST_CHARACTERISTIC_UUID                           = "6e4a0001-b5a3-f393-e0a9-e50e24dcca9e"
SQL_GET_INGREDIENT_LIST_CHARACTERISTIC_UUID                         = "6e4a0002-b5a3-f393-e0a9-e50e24dcca9e"
SQL_GET_QUANTITE_MACHINE_CHARACTERISTIC_UUID                        = "6e4a0003-b5a3-f393-e0a9-e50e24dcca9e"
SQL_GET_ALL_COCKTAIL_COMPOSITION_WITHOUT_BEC_CHARACTERISTIC_UUID    = "6e4a0004-b5a3-f393-e0a9-e50e24dcca9e"
SQL_GET_COCKTAIL_COMPOSITION_WITHOUT_BEC_CHARACTERISTIC_UUID        = "6e4a0005-b5a3-f393-e0a9-e50e24dcca9e"
SQL_GET_COCKTAIL_COMPOSITION_WITH_BEC_CHARACTERISTIC_UUID           = "6e4a0006-b5a3-f393-e0a9-e50e24dcca9e"
MACHINE_SEND_ORDER_CHARACTERISTIC_UUID                              = "6e4a0007-b5a3-f393-e0a9-e50e24dcca9e"
SQL_PUT_COCKTAIL_CHARACTERISTIC_UUID                                = "6e4a0008-b5a3-f393-e0a9-e50e24dcca9e"
SQL_ADD_INGREDIENT_CHARACTERISTIC_UUID                              = "6e4a0009-b5a3-f393-e0a9-e50e24dcca9e"
SQL_MAJ_MACHINE_CHARACTERISTIC_UUID                                 = "6e4a0010-b5a3-f393-e0a9-e50e24dcca9e"
mainloop = None

class TxCharacteristic(Characteristic):
    def __init__(self, bus, index, service, uuid = None):
        if uuid == None : 
            uuid = UART_TX_CHARACTERISTIC_UUID
        Characteristic.__init__(self, bus, index, uuid,
                                ['notify'], service)
        self.notifying = False
        GLib.io_add_watch(sys.stdin, GLib.IO_IN, self.on_console_input)
        self.desc = Descriptor(bus, index,CLIENT_UUID,['notify'], self)
        self.add_descriptor(self.desc)

    def on_console_input(self, fd, condition):
        s = fd.readline()
        if s.isspace():
            pass
        else:
            self.send_tx(s)
        return True
        
    def do_something(self):
        self.send_tx("do action by default")

    def send_tx(self, s):
        if not self.notifying:
            return
        value = []
        print(s)
        for c in s:
            c_encode = c.encode()
            if len(c_encode)>1:
                c_encode = "-".encode()
                print("Caractere ne passant pas l'encodage : ", c)
            value.append(dbus.Byte(c_encode))
        self.PropertiesChanged(GATT_CHRC_IFACE, {'Value': value}, []) 

    def StartNotify(self):
        print("start notify")
        if self.notifying:
            return
        self.notifying = True
        self.do_something()

    def StopNotify(self):
        print("stop notify")
        if not self.notifying:
            return
        self.notifying = False

class RxCharacteristic(Characteristic):
    def __init__(self, bus, index, service, uuid = None):
        if uuid == None : 
            uuid = UART_RX_CHARACTERISTIC_UUID
        Characteristic.__init__(self, bus, index, uuid,
                                ['write'], service)

    def WriteValue(self, value, options):
        print('remote: {}'.format(bytearray(value).decode()))
        

class UartService(Service):
    def __init__(self, bus, index):
        Service.__init__(self, bus, index, UART_SERVICE_UUID, True)
        self.add_characteristic(BLE_GET_ALL_COCKTAIL_COMPOSITION_WITHOUT_BEC_Characteristic(bus, 1, self))
        self.add_characteristic(BLE_GET_INGREDIENT_LIST(bus, 2, self))
        self.add_characteristic(BLE_GET_QUANTITE_MACHINE(bus, 3, self))
        self.add_characteristic(BLE_MACHINE_SEND_ORDER(bus, 4, self))
        self.add_characteristic(BLE_ADD_INGREDIENT(bus, 5, self))
        self.add_characteristic(BLE_MAJ_MACHINE(bus, 6, self))

class Application(dbus.service.Object):
    def __init__(self, bus):
        self.path = '/'
        self.services = []
        dbus.service.Object.__init__(self, bus, self.path)

    def get_path(self):
        return dbus.ObjectPath(self.path)

    def add_service(self, service):
        self.services.append(service)

    @dbus.service.method(DBUS_OM_IFACE, out_signature='a{oa{sa{sv}}}')
    def GetManagedObjects(self):
        response = {}
        for service in self.services:
            response[service.get_path()] = service.get_properties()
            chrcs = service.get_characteristics()
            for chrc in chrcs:
                response[chrc.get_path()] = chrc.get_properties()
        return response

class UartApplication(Application):
    def __init__(self, bus):
        Application.__init__(self, bus)
        self.add_service(UartService(bus, 0))

class UartAdvertisement(Advertisement):
    def __init__(self, bus, index):
        Advertisement.__init__(self, bus, index, 'peripheral')
        self.add_service_uuid(UART_SERVICE_UUID)
        self.add_local_name(LOCAL_NAME)
        self.include_tx_power = True

def find_adapter(bus):
    remote_om = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, '/'),
                               DBUS_OM_IFACE)
    objects = remote_om.GetManagedObjects()
    for o, props in objects.items():
        if LE_ADVERTISING_MANAGER_IFACE in props and GATT_MANAGER_IFACE in props:
            return o
        print('Skip adapter:', o)
    return None


class BLE_GET_ALL_COCKTAIL_COMPOSITION_WITHOUT_BEC_Characteristic(TxCharacteristic):
    def __init__(self, bus, index, service):
        TxCharacteristic.__init__(self, bus, index, service, SQL_GET_ALL_COCKTAIL_COMPOSITION_WITHOUT_BEC_CHARACTERISTIC_UUID)

    def do_something(self):
        self.send_tx(formalize_data_list(mysql_get_all_cocktail_composition_without_bec))
        
class BLE_GET_INGREDIENT_LIST(TxCharacteristic):
    def __init__(self, bus, index, service):
        TxCharacteristic.__init__(self, bus, index, service, SQL_GET_INGREDIENT_LIST_CHARACTERISTIC_UUID)

    def do_something(self):
        self.send_tx(formalize_data_list(mysql_get_ingredient_list))
        
class BLE_GET_QUANTITE_MACHINE(TxCharacteristic):
    def __init__(self, bus, index, service):
        TxCharacteristic.__init__(self, bus, index, service, SQL_GET_QUANTITE_MACHINE_CHARACTERISTIC_UUID)

    def do_something(self):
        self.send_tx(formalize_data_list(mysql_get_quantite_machine))

class BLE_ADD_INGREDIENT(RxCharacteristic):
    def __init__(self, bus, index, service):
        RxCharacteristic.__init__(self, bus, index, service, SQL_ADD_INGREDIENT_CHARACTERISTIC_UUID)

    def WriteValue(self, value, options):
        ingredient = format(bytearray(value).decode())
        mysql_add_ingredient(ingredient)

class BLE_MAJ_MACHINE(RxCharacteristic):
    def __init__(self, bus, index, service):
        RxCharacteristic.__init__(self, bus, index, service, SQL_MAJ_MACHINE_CHARACTERISTIC_UUID)

    def WriteValue(self, value, options):
        print(format(bytearray(value).decode()))
        maj = unformalize_data_list(format(bytearray(value).decode()))
        print(maj)
        mysql_maj_machine(maj)

class BLE_MACHINE_SEND_ORDER(RxCharacteristic):
    def __init__(self, bus, index, service):
        RxCharacteristic.__init__(self, bus, index, service, MACHINE_SEND_ORDER_CHARACTERISTIC_UUID)

    def WriteValue(self, value, options):
        print('cocktail commandé: {}'.format(bytearray(value).decode()))
        data = unformalize_data_list(format(bytearray(value).decode()))
        commande = mysql_get_composition_for_machine(data)
        print(commande)
        prepare_commande(commande)

def main():
    global mainloop
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SystemBus()
    adapter = find_adapter(bus)
    if not adapter:
        print('BLE adapter not found')
        return

    service_manager = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, adapter),GATT_MANAGER_IFACE)
    ad_manager = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, adapter),LE_ADVERTISING_MANAGER_IFACE)

    app = UartApplication(bus)
    adv = UartAdvertisement(bus, 0)

    mainloop = GLib.MainLoop()

    service_manager.RegisterApplication(app.get_path(), {},
                                        reply_handler=register_app_cb,
                                        error_handler=register_app_error_cb)
    ad_manager.RegisterAdvertisement(adv.get_path(), {},
                                     reply_handler=register_ad_cb,
                                     error_handler=register_ad_error_cb)
    try:
        mainloop.run()
    except KeyboardInterrupt:
        adv.Release()

if __name__ == '__main__':
    main()
