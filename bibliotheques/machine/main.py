from multiprocessing.connection import wait
import RPi.GPIO as gpio
import sys

increment = 0
stop = 0

gpioPompe1 = 0
gpioPompe2 = 3
gpioPompe3 = 4

gpioElectrovanne1 = 1
gpioElectrovanne2 = 2
gpioElectrovanne3 = 5
gpioElectrovanne4 = 6
gpioElectrovanne5 = 7
gpioElectrovanne6 = 8

gpioDebitmetre1 = 1
gpioDebitmetre2 = 2
gpioDebitmetre3 = 3

pompe1 = gpio.PWM(gpioPompe1,100)
pompe2 = gpio.PWM(gpioPompe2,100)
pompe3 = gpio.PWM(gpioPompe3,100)

def gpioSetup():
    gpio.setmode(gpio.BCM)
    #gpio POMPES
    gpio.setup(gpioPompe1, gpio.OUT, initial=gpio.LOW)
    gpio.setup(gpioPompe2, gpio.OUT, initial=gpio.LOW)
    gpio.setup(gpioPompe3, gpio.OUT, initial=gpio.LOW)
    #gpio EV
    gpio.setup(gpioElectrovanne1, gpio.OUT, initial=gpio.LOW)
    gpio.setup(gpioElectrovanne2, gpio.OUT, initial=gpio.LOW)
    gpio.setup(gpioElectrovanne3, gpio.OUT, initial=gpio.LOW)
    gpio.setup(gpioElectrovanne4, gpio.OUT, initial=gpio.LOW)
    gpio.setup(gpioElectrovanne5, gpio.OUT, initial=gpio.LOW)
    gpio.setup(gpioElectrovanne6, gpio.OUT, initial=gpio.LOW)
    #gpio DEBIMETRE
    gpio.setup(gpioDebitmetre1, gpio.IN)
    gpio.setup(gpioDebitmetre2, gpio.IN)
    gpio.setup(gpioDebitmetre3, gpio.IN)

def main(commande: tuple):
    increment = 0

    for k in len(commande):
        id_bec = commande[k][0]

        if increment < commande[k][1]:
            gpio.output(pompe1, gpio.HIGH)
            