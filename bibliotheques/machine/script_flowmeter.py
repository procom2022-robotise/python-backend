from multiprocessing.connection import wait
import RPi.GPIO as GPIO  
from time import sleep     # this lets us have a time delay (see line 12)  
import sys

increment = 0
nbrBoucle = 0
nbrCyclesPompage = 0
stop = 0
currentQuantity = -1
pwmMotor = 0

args = []
nbrArgs = len(sys.argv)  # Ordre : Pompe1 Pompe2 Pompe3 EV1 EV2 EV3 EV4 EV5 EV6 EV7
Pompe1 = 0
EV1 = 1
EV2 = 2
Pompe2 = 3
Pompe3 = 4
EV3 = 5
EV4 = 6
EV5 = 7
EV6 = 8
EV7 = 9

args = [1,1,0]


def prepare_commande(commande):
    if not commande:
        print("Cocktail incomplet - ingrédient non présent")
    else: 
        for bec, quantite in commande:
            #setupGpio()
            print("in prepare commande, bec : "+str(bec)+", quantite : "+ str(quantite))
            #startLine(bec, quantite, 0)

# Setup des composants
def setupGpio():
    global pwmMotor
    GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering  
    GPIO.setup(25, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)    # DEBITMETRE  
    GPIO.setup(24, GPIO.OUT, initial=GPIO.LOW)   # ELECTROVANNE 1
    GPIO.setup(21, GPIO.OUT, initial=GPIO.LOW)   # ELECTROVANNE 2
    GPIO.setup(23, GPIO.OUT)   # POMPE
    pwmMotor = GPIO.PWM(23,300)

# Démarrage des composants requis
def startPompage(quantite):
    global increment
    global pwmMotor

    PWM = 0
    increment = 1
    while PWM <= 50:
        pwmMotor.start(PWM)
        print("PWM =", PWM)
        PWM += 5
        sleep(0.1)

    #sleep(0.3)

def stopLine():
    global pwmMotor
    pwmMotor.stop()
    #GPIO.remove_event_detect(25)
    GPIO.cleanup()         # clean up after yourself  
    print("GPIO cleaned")

def startLine(bec, quantite, amorcage): # quantité en cl (int), bec (int)

    global currentQuantity
    global stop
    currentQuantity = quantite
    print("Start : Ligne", bec)
    print("Quantité voulue : ", quantite)

    openMotorPompe1 = 0
    stop = 0

    if not amorcage:
        if bec == 1:
            GPIO.output(24, GPIO.HIGH)
            print("Electrovanne1 ouverte")
            startMotorPompe1 = 1

        elif bec == 2:
            GPIO.output(21, GPIO.HIGH)
            print("Electrovanne2 ouverte")
            startMotorPompe1 = 1

        else:  
            print("NON IMPLEMENTE")
    else:
        print("Phase amorcage")
        GPIO.output(24, GPIO.HIGH)
        print("Electrovanne1 ouverte")

        GPIO.output(21, GPIO.HIGH)
        print("Electrovanne2 ouverte")

        startMotorPompe1 = 0
        sleep(10)

    # Define a threaded callback function to run in another thread when events are detected  
    def flowmeterTrigger(channel):  
        global nbrBoucle
        global increment
        global nbrCyclesPompage
        global currentQuantity
        global stop

        # Si la quantité désirée a été pompée
        if increment == currentQuantity:
            
            # Arrêt de tous les composants

            #pwmMotor.stop()
            print("Pompe fermée")

            GPIO.output(24, GPIO.LOW)
            print("Electrovanne 1 fermée")

            GPIO.output(21, GPIO.LOW)
            print("Electrovanne 2 fermée")
            stopLine()
            stop = 1

            
        # Tant qu'il manque du liquide on continue de pomper
        elif GPIO.input(25):     # if port 25 == 1  
            print("Liquide détecté")  
            increment+=1
            print(increment)  
    
    # Ajout du trigger débitmètre 
    GPIO.add_event_detect(25, GPIO.RISING, callback=flowmeterTrigger, bouncetime=5) 
    
    if startMotorPompe1:
        startPompage(currentQuantity)
 

    #GPIO.output(23, GPIO.HIGH)

    try:
        while not stop:
            pass
    finally:
        print("CLEANUP")
        GPIO.cleanup() 

    print("Fin du programme")
    increment = 0

if __name__=="__main__":
    #setupGpio()
    #stopLine()
    setupGpio()
    startLine(2, 100, 0)
    #setupGpio()
    #startLine(2, 30, 0)
    
