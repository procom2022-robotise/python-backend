# Introduction

Le projet Robotise est découpé en trois parties, une application Android, une base de données et du code Python.

Le code Python réalise toutes les interactions entre la base de données et l'application Android, mais il sert aussi au pilotage des différents composants de la machine.

Ce dépôt contient uniquement la partie Python.

# Organisation du dépot

## Introduction

Ce dépôt est organisé de la même manière que le projet global, à savoir : 

1. `une partie traitement SGBD (SQL dans notre cas)`

2. `une partie communication via l'utilisation de la technologie BLE`

3. `Une partie pilotage en utilisant les ports GPIO du Raspberry PI`

Afin de simplifier le code et permettre une meilleure maintenabilité/lisibilité, nous avons fait le choix de séparer les trois parties en trois bibliothèques de fonctions.

Puis chaque fonction est appelée dans le fichier main.py qui coordonne l'intégralité des opérations.

## Tree du projet

```
python-backend
├── README.md
├── bibliotheques
│   ├── ble
│   │   ├── advertisement.py
│   │   ├── conversion_UUID_BLE.txt
│   │   ├── gatt_server.py
│   │   └── main.py
│   ├── machine
│   │   ├── main.py
│   │   └── script_flowmeter.py
│   └── sql
│       └── sql.py
├── main.py
├── requirements.txt
└── tests
    ├── test_sql_formalize.py
    └── test_sql_req.py
```

## Détail de chaque dossier

Chacune des trois parties est localisée dans le dossier `bibliotheques`.

Le dossier `BLE` regroupe toutes les fonctions nécessaires à la communication entre la Raspberry et l'application Android

Le dossier `sql` regroupe toutes les fonctions utilisées pour lire ou écrire dans la base de données.

Le dossier `machine` contient les différentes fonctions permettant le pilotage des éléments physiques de la machine (pompe, capteur, etc.).

# Explication de chaque partie

## Partie BLE

### Dossier rattaché : `python-backend/bibliotheques/ble`

### Organisation

Cette bibliothèque contient 3 fichiers :

1. `advertisement.py` : Contiens les classes nécessaires à l'émission des différents UUIDs après la création du serveur.

2. `gatt_server.py` : Contiens les classes nécessaires à la création du serveur et l'initialisation de ses différents paramètres.

3. `main.py` : Initialise le serveur BLE en utilisant les classes réalisées précédemment.

### Fonctionnement

Les concepts abordés dans la partie suivante sont expliqués ici : https://www.bluetooth.com/bluetooth-resources/intro-to-bluetooth-low-energy/

Les fichiers `advertisement.py` et `gatt_server.py` ne sont pas de nous et à ce titre, ne feront pas l'objet d'une documentation (Crédit : https://github.com/bluez/bluez).

Le fichier `main.py` utilise `advertisement.py` et `gatt_server.py` pour créer le serveur BLE.

On dispose de deux classes : 

1. `TxCharacteristic` qui gère la communication dans le sens Raspberry PI -> Téléphone

2. `RxCharacteristic` qui gère la communication dans le sens Téléphone -> Raspberry PI

Ensuite on redéfini des classe pour chaque opération que l'on veut réaliser (Mise a jour de base de donnée, commande, etc), chacune est identifié par un UUID référencé en début de fichier.

Tous les UUID ne sont pas utilisés mais ils permettent de couvrir l'intégralité des cas imaginés.

## Partie SQL

### Dossier rattaché : `python-backend/bibliotheques/sql`

### Organisation

Cette bibliothèque contient 1 fichier :

`main.py` : Contiens toutes les fonctions qui vont interagir avec la base de données.

### Fonctionnement

Voici une description du rôle de chaque fonction de la bibliothèque :

1. `mysql_get_cocktail_list()` : Permet de récupérer toutes les colonnes de la table cocktail.

2. `mysql_get_ingredient_list()` : Permet de récupérer toutes les colonnes de la table ingredient.

3. `mysql_get_quantite_machine()` : Permet de récupérer toutes les colonnes de la table machine.

4. `mysql_get_all_cocktail_composition_without_bec(id_cocktail)` : Prend en entrée l'identifiant du cocktail, réalise une jointure entre les tables `composition`, `cocktail` et `ingredient` et retourne id_cocktail, nom_cocktail, id_ingredient, nom_ingredient et quantite.

5. `mysql_get_all_cocktail_composition_with_bec(id_cocktail)` : Prend en entrée l'identifiant du cocktail, réalise une jointure entre les tables `composition`, `cocktail` et `ingredient` et retourne id_cocktail, nom_cocktail, id_ingredient, nom_ingredient, quantite et id_bec.

6. `mysql_get_bec_from_ingredient(id_ingredient)` : Prend en entrée l'identifiant d'un ingredient, retourne le numéro de bec associé.

7. `mysql_add_ingredient(nom_ingredient)` : Prend en entrée le nom d'un ingrédient, puis l'ajoute dans la base de donnée. L'identifiant de l'ingrédient est automatique et le numéro de bec est initialisé à NULL.

8. `mysql_maj_machine(maj)` : Prend en entrée une liste de tuples au format : `'machine.id_bec','ingredient.id_ingredient','machine.quantite'`, dans la table ingredient passe toute la colonne id_bec à NULL (pour ne pas avoir de doublon), pour chaque identifiant de bec dans la table machine, rajoute la quantite passée en argument et enfin, dans la table ingrédient ajoute le numéro de bec correspondant à l'ingrédient.

9. `mysql_get_composition_for_machine(commande)` : Prend en entrée une liste de tuples au format : `'id_cocktail','id_ingredient','quantite'` réalise une itération dans la liste pour retourner une liste de tuples au format `'id_bec','quantite'` ou id_bec représente l'identifiant du bec lié à l'identifiant de l'ingredient passé en argument. Si un ingrédient ne dispose pas de numéro de bec, il n'est pas présent dans la machine donc on sort de la boucle et on retourne None.

10. `formalize_data_list(function)` : Prend en entrée une fonction (l'une des fonctions ci-dessus) et convertis la sortie soit un tuple ou une liste de tuples en chaine textuel (`x,y,z/a,b,c`) afin de permettre l'envoie en Bluetooth.

11. `unformalize_data_list(initial_data)` : Inverse de la fonction précédente, prend en entrée un string au format `x,y,z/a,b,c` et reconvertis en tuple ou liste de tuples selon les cas.

## Partie pilotage machine

### Dossier rattaché : `python-backend/bibliotheques/machine`

### Organisation

TODO

### Fonctionnement

TODO